import streamlit as st
from datetime import datetime
def main():

    username = st.text_input("Username")
    password = st.text_input("Password", type="password") 
    if username == "admin" and password == "password":
            st.success("Logged in as {}".format(username))
            # Redirect to another page after successful login
            # Simulate redirect delay
            import time
            time.sleep(2)
            # Redirect to another page
            st.experimental_rerun()
    else:
            st.error("Invalid username or password")

if __name__ == "__main__":
    main()